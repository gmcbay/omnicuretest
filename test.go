package main

import (
	"C"
	"bitbucket.org/gmcbay/omnicure"
	"fmt"
	"log"
	"time"
)

func main() {
	s2000, err := omnicure.NewS2000("COM3")

	if err != nil {
		log.Fatalf("Err: %v\n", err)
	}

	// connect
	if res, err := s2000.SendCommand("CONN"); err == nil {
		fmt.Printf("Result? %s\n", res)
	} else {
		fmt.Printf("Read Err: %v\n", err)
	}

	// set iris level to 50
	if res, err := s2000.SendCommand("SIL50"); err == nil {
		fmt.Printf("Result? %s\n", res)
	} else {
		fmt.Printf("Read Err: %v\n", err)
	}

	// open shutter
	if res, err := s2000.SendCommand("OPN"); err == nil {
		fmt.Printf("Result? %s\n", res)
	} else {
		fmt.Printf("Read Err: %v\n", err)
	}

	time.Sleep(3 * time.Second)

	// close shutter
	if res, err := s2000.SendCommand("CLS"); err == nil {
		fmt.Printf("Result? %s\n", res)
	} else {
		fmt.Printf("Read Err: %v\n", err)
	}

	// disconnect
	if res, err := s2000.SendCommand("DCON"); err == nil {
		fmt.Printf("Result? %s\n", res)
	} else {
		fmt.Printf("Read Err: %v\n", err)
	}

	s2000.Cleanup()
}
